import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Usuario } from "../entity/Usuario";

export const postInicio = async (
  req: Request,
  res: Response
): Promise<Response> => {
  return connection
    .then(async (connection) => {
      const usuario = await connection
      .getRepository(Usuario)
      .createQueryBuilder("usuario")
      .where("usuario.correo = :c AND usuario.password = :p"
      , {c: req.body.correo, p:req.body.password})
      .getOne();

      if (usuario) {
        return res.status(200).send({userid: usuario.userid});
      }
      return res.status(404).send({mensaje: "no coincide correo con password"});
    })
    .catch((error) => {
      return res.status(404).json(error).send();
    });
};

