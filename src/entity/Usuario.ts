import { Entity, Column, PrimaryGeneratedColumn, Unique, ManyToMany, JoinTable, OneToMany } from "typeorm";
import { Tema } from "./Tema";
import { IsEmail } from "class-validator";
import { Noticia } from "./Noticia";
import { Comentario } from "./Comentario";

@Entity()
@Unique(["correo"])
export class Usuario {
  @PrimaryGeneratedColumn()
  userid: number;

  @Column()
  @IsEmail()
  correo: string;

  @Column()
  nombre: string;

  @Column()
  apellido: string;

  @Column()
  password: string;

  @ManyToMany(type => Tema)
  @JoinTable()
  temas: Tema[];

  @OneToMany(type => Noticia, noticia => noticia.usuario)
  noticias: Noticia[];

  @OneToMany(type => Comentario, comentario => comentario.usuario)
  comentarios: Comentario[];

  @ManyToMany(type => Noticia, noticia => noticia.usuarios)
  @JoinTable()
  noticiasLike: Noticia[];

}
