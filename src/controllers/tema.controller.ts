import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Usuario } from "../entity/Usuario";
import { Tema } from "../entity/Tema";

export const getTemas = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
    .then(async (connection) => {
        const temas = await connection
        .getRepository(Tema)
        .createQueryBuilder("tema")
        .getMany();
    
        if (temas){

            return res.status(200).send({temas: temas});
        }
        return res.status(404).send({mensaje: "no se recuperaron temas"})
    }).catch((error)=> {
        return res.status(404).json(error).send();
    });
};

export const postTemasUsuario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .leftJoinAndSelect("usuario.temas", "temas")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();

            if (usuario) {
                return res.status(200).send({ temas: usuario.temas });
            }
            return res.status(404).send({ mensaje: "no existe usuario" });
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const postTemaUsuario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const tema = await connection
                .getRepository(Tema)
                .createQueryBuilder("tema")
                .where("tema.temaid = :id", { id: req.body.temaid })
                .getOne();
            if (!tema) {
                return res.status(404).send({ mensaje: "no existe tema" });
            }
            const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .leftJoinAndSelect("usuario.temas", "temas")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();
            if (!usuario) {
                return res.status(404).send({ mensaje: "no existe usuario" });
            }
            usuario.temas.push(tema);
            await connection.manager.save(usuario);
            return res.status(200).send();
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const deleteTemaUsuario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            await connection
            .createQueryBuilder()
            .relation(Usuario, "temas")
            .of([{userid:req.body.userid}])
            .remove({temaid:req.body.temaid});
            return res.status(200).send();
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};