import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Usuario } from "../entity/Usuario";
import { Noticia } from "../entity/Noticia";

export const postTimeLineUsuario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .leftJoinAndSelect("usuario.temas", "temas")
                .leftJoinAndSelect("usuario.noticiasLike", "noticiasLike")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();

            if (!usuario) {
                return res.status(404).send({ mensaje: "no existe usuario" });
            }
            let idtemas:number[] = [];
            usuario.temas.forEach(element => {
                idtemas.push(element.temaid);
            });
            const noticias = await connection
            .getRepository(Noticia)
            .createQueryBuilder("noticia")
            .leftJoinAndSelect("noticia.tema", "tema")
            .leftJoinAndSelect("noticia.usuario", "usuario")
            .leftJoinAndSelect("noticia.comentarios", "comentarios")
            .leftJoinAndSelect("noticia.usuarios", "usuarios")
            .where("noticia.tema IN(:...temas)", {temas: idtemas})
            .orderBy("noticia.fecha", "DESC")
            .getMany();
            return res.status(200).send({ noticias: noticias, usuario: usuario });
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};



