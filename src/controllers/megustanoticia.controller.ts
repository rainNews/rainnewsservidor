import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Usuario } from "../entity/Usuario";
import { Noticia } from "../entity/Noticia";

export const postNoticiaUsuariosLikes = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
    .then(async (connection) => {
        const noticia = await connection
        .getRepository(Noticia)
        .createQueryBuilder("noticia")
        .leftJoinAndSelect("noticia.usuarios", "usuarios")
        .where("noticia.noticiaid = :id", {id: req.body.noticiaid})
        .getOne();
        if (noticia) {
            return res.status(200).send({ usuarios: noticia.usuarios });
        }
        return res.status(404).send({ mensaje: "no existe noticia" });
    }).catch((error) => {
        return res.status(404).json(error).send();
    });
};


export const postNoticiasLikes = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .leftJoinAndSelect("usuario.noticiasLike", "noticiasLike")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();

            if (usuario) {
                return res.status(200).send({ noticiasLike: usuario.noticiasLike });
            }
            return res.status(404).send({ mensaje: "no existe usuario" });
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const postNoticiaLike = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const noticia = await connection
                .getRepository(Noticia)
                .createQueryBuilder("noticia")
                .where("noticia.noticiaid = :id", { id: req.body.noticiaid })
                .getOne();
            if (!noticia) {
                return res.status(404).send({ mensaje: "no existe noticia" });
            }
            const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .leftJoinAndSelect("usuario.noticiasLike", "noticiasLike")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();
            if (!usuario) {
                return res.status(404).send({ mensaje: "no existe usuario" });
            }
            usuario.noticiasLike.push(noticia);
            await connection.manager.save(usuario);
            return res.status(200).send();
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const deleteNoticiaLike = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            await connection
            .createQueryBuilder()
            .relation(Usuario, "noticiasLike")
            .of([{userid:req.body.userid}])
            .remove({noticiaid:req.body.noticiaid});
            return res.status(200).send();
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};