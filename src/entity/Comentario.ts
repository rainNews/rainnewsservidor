import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Noticia } from "./Noticia";
import { Usuario } from "./Usuario";

@Entity()
export class Comentario {
    @PrimaryGeneratedColumn()
    comentarioid: number;

    @Column()
    comentario: string;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    fecha: string;

    @ManyToOne(type=> Noticia, noticia => noticia.comentarios)
    noticia: Noticia;

    @ManyToOne(type=> Usuario, usuario => usuario.comentarios)
    usuario: Usuario;
}
