"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Tema_1 = require("./Tema");
var Usuario_1 = require("./Usuario");
var Comentario_1 = require("./Comentario");
var Noticia = /** @class */ (function () {
    function Noticia() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Noticia.prototype, "noticiaid", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Noticia.prototype, "noticia", void 0);
    __decorate([
        typeorm_1.Column({ type: "timestamp", default: function () { return "CURRENT_TIMESTAMP"; } }),
        __metadata("design:type", String)
    ], Noticia.prototype, "fecha", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Tema_1.Tema; }, function (tema) { return tema.noticias; }),
        __metadata("design:type", Tema_1.Tema)
    ], Noticia.prototype, "tema", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Usuario_1.Usuario; }, function (usuario) { return usuario.noticias; }),
        __metadata("design:type", Usuario_1.Usuario)
    ], Noticia.prototype, "usuario", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Comentario_1.Comentario; }, function (comentario) { return comentario.noticia; }),
        __metadata("design:type", Array)
    ], Noticia.prototype, "comentarios", void 0);
    __decorate([
        typeorm_1.ManyToMany(function (type) { return Usuario_1.Usuario; }, function (usuario) { return usuario.noticiasLike; }),
        __metadata("design:type", Array)
    ], Noticia.prototype, "usuarios", void 0);
    Noticia = __decorate([
        typeorm_1.Entity()
    ], Noticia);
    return Noticia;
}());
exports.Noticia = Noticia;
