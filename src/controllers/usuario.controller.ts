import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Usuario } from "../entity/Usuario";

export const getUsuario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();
            if (usuario) {
                return res.status(200).send({ usuario: usuario});
            }
            return res.status(404).send({ mensaje: "no existe usuario" });
        });
};

export const postUsuario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
    .then(async (connection) => {
        const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();
            if (usuario) {
                usuario.apellido = req.body.apellido;
                usuario.correo = req.body.correo;
                usuario.nombre = req.body.nombre;
                usuario.password = req.body.password;
                await connection.manager.save(usuario);
                return res.status(200).send();
            }
            return res.status(404).send({ mensaje: "no existe usuario" });
    });
};