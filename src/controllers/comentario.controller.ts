import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Usuario } from "../entity/Usuario";
import { Noticia } from "../entity/Noticia";
import { Comentario } from "../entity/Comentario";

export const postComentarios = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const noticia = await connection
                .getRepository(Noticia)
                .createQueryBuilder("noticia")
                .leftJoinAndSelect("noticia.comentarios", "comentarios")
                .where("noticia.noticiaid = :id", { id: req.body.noticiaid })
                .getOne();

            if (noticia) {
                return res.status(200).send({ noticia: noticia });
            }
            return res.status(404).send({ mensaje: "no existe noticia" });
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const postComentario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const noticia = await connection
                .getRepository(Noticia)
                .createQueryBuilder("noticia")
                .leftJoinAndSelect("noticia.comentarios", "comentarios")
                .where("noticia.noticiaid = :id", { id: req.body.noticiaid })
                .getOne();
            if (!noticia) {
                return res.status(404).send({ mensaje: "no existe noticia" });
            }
            const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .leftJoinAndSelect("usuario.comentarios", "comentarios")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();

                
            if (!usuario) {
                return res.status(404).send({ mensaje: "no existe usuario" });
            }

            const comentario = new Comentario();
            comentario.comentario = req.body.comentario;
            await connection.manager.save(comentario);

            usuario.comentarios.push(comentario);
            await connection.manager.save(usuario);

            noticia.comentarios.push(comentario)
            await connection.manager.save(noticia);
            return res.status(200).send();
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const postUpdateComentario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection.
        then(async (connection) => {
            const comentario = await connection
                .getRepository(Comentario)
                .createQueryBuilder("comentario")
                .where("comentario.comentarioid = :id", { id: req.body.comentarioid })
                .getOne();

            if (!comentario) {
                return res.status(404).send({ mensaje: "no existe comentario" });
            }
            comentario.comentario = req.body.comentario;
            await connection.manager.save(comentario);
            return res.status(200).send();
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const getComentario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection.
        then(async (connection) => {
            const comentario = await connection
                .getRepository(Comentario)
                .createQueryBuilder("comentario")
                .where("comentario.comentarioid = :id", { id: req.body.comentarioid })
                .getOne();

            if (!comentario) {
                return res.status(404).send({ mensaje: "no existe comentario" });
            }
            return res.status(200).send({ comentario: comentario});
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};