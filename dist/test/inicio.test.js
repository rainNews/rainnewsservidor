"use strict";
var supertest = require("supertest");
var app = require("../index");
var chai = require("chai");
chai.should();
var expect = chai.expect;
var api = supertest.agent(app);
describe("/post registroUsuario", function () {
    it("should not add a new user", function (done) {
        api
            .post("/registroUsuario")
            .send({
            correo: 'pruebacorreo',
            nombre: 'prueba',
            apellido: 'prueba',
            password: 'prueba'
        }).end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post inicio", function () {
    it("should connect to the app", function (done) {
        api
            .post("/inicio")
            .send({
            correo: "pepito",
            password: "pepito"
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post noticias usuario", function () {
    it("should connect to the app", function (done) {
        api
            .post("/postNoticiasUsuario")
            .send({
            id: "pepito"
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post timeline", function () {
    it("should connect to the app", function (done) {
        api
            .post("/timeline")
            .send({
            id: "pepito" // id del usuario
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post tema", function () {
    it("should connect to the app", function (done) {
        api
            .post("/temasUsuario")
            .send({
            id: 1 // id del usuario
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post me gusta en publicacion", function () {
    it("should connect to the app", function (done) {
        api
            .post("/postNoticiaUsuariosLikes")
            .send({
            id: 1 // id de noticia
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post modificar perfil", function () {
    it("should not add a new user", function (done) {
        api
            .post("/postUsuario")
            .send({
            correo: 'pruebacorreo',
            nombre: 'prueba',
            apellido: 'prueba',
            password: 'prueba'
        }).end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/get comentario", function () {
    it("should connect to the app", function (done) {
        api
            .post("/getComentario")
            .send({
            id: "1"
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post comentarios", function () {
    it("should connect to the app", function (done) {
        api
            .post("/postComentarios")
            .send({
            id: "1"
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post comentario", function () {
    it("should connect to the app", function (done) {
        api
            .post("/postComentario")
            .send({
            id: "1"
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post UpdateComentario", function () {
    it("should connect to the app", function (done) {
        api
            .post("/postUpdateComentario")
            .send({
            id: "1"
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post delete noticia usuario", function () {
    it("should connect to the app", function (done) {
        api
            .post("/deleteNoticiaUsuario")
            .send({
            userid: "pepito",
            noticiaid: 1
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post update noticia", function () {
    it("should connect to the app", function (done) {
        api
            .post("/postUpdateNoticia")
            .send({
            id: 1
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
describe("/post enviar correo noticia usuario", function () {
    it("should connect to the app", function (done) {
        api
            .post("/postNoticiaUsuario")
            .send({
            id: 1
        })
            .end(function (err, res) {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
