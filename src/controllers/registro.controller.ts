import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Usuario } from "../entity/Usuario";
import { validate } from "class-validator";

export const postRegistroUsuario = async (
  req: Request,
  res: Response
): Promise<Response> => {
  return connection
    .then(async (connection) => {
      const usuario = new Usuario();
      usuario.correo = req.body.correo;
      usuario.nombre = req.body.nombre;
      usuario.apellido = req.body.apellido;
      usuario.password = req.body.password;

      const errors = await validate(usuario);
      if (errors.length > 0) {
        return res.status(404).json(errors).send();
      }
      await connection.manager.save(usuario);

      return res.status(200).send();
    })
    .catch((error) => {
      return res.status(404).json(error).send();
    });
};
