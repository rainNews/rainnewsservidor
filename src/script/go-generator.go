// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	idcomentario := 1
	err := ioutil.WriteFile("temp.txt", []byte("-- comentarios\n"), 0644)
	if err != nil {
		log.Fatal(err)
	}
	file, err := os.OpenFile("temp.txt", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer file.Close()
	for idnoticia := 1; idnoticia < 604; idnoticia++ {
		if _, err := file.WriteString(fmt.Sprintf("-- noticia %d\n", idnoticia)); err != nil {
			log.Fatal(err)
		}
		for idusuario := 1; idusuario < 15; idusuario++ {
			for i := 0; i < 2; i++ {
				contenido := "INSERT INTO `rainnews`.`comentario`\n"
				contenido += "(`comentario`, `usuarioUserid`, `noticiaNoticiaid`)\n"
				contenido += "VALUES\n"
				contenido += fmt.Sprintf("(\"Este es un comentario %d-%d-%d\", %d, %d);\n", idusuario, idnoticia, idcomentario, idusuario, idnoticia)
				if _, err := file.WriteString(contenido); err != nil {
					log.Fatal(err)
				}
				idcomentario++
			}
		}
	}
}
