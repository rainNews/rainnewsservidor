"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Tema_1 = require("./Tema");
var class_validator_1 = require("class-validator");
var Noticia_1 = require("./Noticia");
var Comentario_1 = require("./Comentario");
var Usuario = /** @class */ (function () {
    function Usuario() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Usuario.prototype, "userid", void 0);
    __decorate([
        typeorm_1.Column(),
        class_validator_1.IsEmail(),
        __metadata("design:type", String)
    ], Usuario.prototype, "correo", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Usuario.prototype, "nombre", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Usuario.prototype, "apellido", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Usuario.prototype, "password", void 0);
    __decorate([
        typeorm_1.ManyToMany(function (type) { return Tema_1.Tema; }),
        typeorm_1.JoinTable(),
        __metadata("design:type", Array)
    ], Usuario.prototype, "temas", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Noticia_1.Noticia; }, function (noticia) { return noticia.usuario; }),
        __metadata("design:type", Array)
    ], Usuario.prototype, "noticias", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Comentario_1.Comentario; }, function (comentario) { return comentario.usuario; }),
        __metadata("design:type", Array)
    ], Usuario.prototype, "comentarios", void 0);
    __decorate([
        typeorm_1.ManyToMany(function (type) { return Noticia_1.Noticia; }, function (noticia) { return noticia.usuarios; }),
        typeorm_1.JoinTable(),
        __metadata("design:type", Array)
    ], Usuario.prototype, "noticiasLike", void 0);
    Usuario = __decorate([
        typeorm_1.Entity(),
        typeorm_1.Unique(["correo"])
    ], Usuario);
    return Usuario;
}());
exports.Usuario = Usuario;
