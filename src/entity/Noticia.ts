import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, ManyToMany } from "typeorm";
import { Tema } from "./Tema";
import { Usuario } from "./Usuario";
import { Comentario } from "./Comentario";

@Entity()
export class Noticia {
    @PrimaryGeneratedColumn()
    noticiaid: number;

    @Column()
    noticia: string;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    fecha: string;

    @ManyToOne(type => Tema, tema => tema.noticias)
    tema: Tema;

    @ManyToOne(type => Usuario, usuario => usuario.noticias)
    usuario: Usuario;

    @OneToMany(type => Comentario, comentario => comentario.noticia)
    comentarios: Comentario[];

    @ManyToMany(type => Usuario, usuario => usuario.noticiasLike)
    usuarios: Usuario[];

}
