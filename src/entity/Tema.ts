import { Entity, Column, PrimaryGeneratedColumn, Unique, OneToMany } from "typeorm";
import { Noticia } from "./Noticia";

@Entity()
@Unique(["nombre"])
export class Tema {
  @PrimaryGeneratedColumn()
  temaid: number;

  @Column()
  nombre: string;

  @Column()
  descripcion: string;

  @OneToMany(type => Noticia, noticia => noticia.tema)
  noticias: Noticia[];
}
