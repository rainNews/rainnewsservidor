"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Noticia_1 = require("./Noticia");
var Usuario_1 = require("./Usuario");
var Comentario = /** @class */ (function () {
    function Comentario() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Comentario.prototype, "comentarioid", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Comentario.prototype, "comentario", void 0);
    __decorate([
        typeorm_1.Column({ type: "timestamp", default: function () { return "CURRENT_TIMESTAMP"; } }),
        __metadata("design:type", String)
    ], Comentario.prototype, "fecha", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Noticia_1.Noticia; }, function (noticia) { return noticia.comentarios; }),
        __metadata("design:type", Noticia_1.Noticia)
    ], Comentario.prototype, "noticia", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Usuario_1.Usuario; }, function (usuario) { return usuario.comentarios; }),
        __metadata("design:type", Usuario_1.Usuario)
    ], Comentario.prototype, "usuario", void 0);
    Comentario = __decorate([
        typeorm_1.Entity()
    ], Comentario);
    return Comentario;
}());
exports.Comentario = Comentario;
