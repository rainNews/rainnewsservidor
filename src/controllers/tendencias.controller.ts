import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Tema } from "../entity/Tema";

export const getTendencias = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
    .then(async (connection) => {
        const temas = await connection
        .getRepository(Tema)
        .createQueryBuilder("tema")
        .leftJoinAndSelect("tema.noticias", "noticias")
        .getMany();
    
        if (!temas){
            return res.status(404).send({mensaje: "no se recuperaron temas para tendencias"});
        }
        const tendencias:Array<Tema> = temas.sort((a: Tema, b:Tema) => a.noticias.length - b.noticias.length).reverse().slice(0, req.body.limite);
        return res.status(200).send({tendencias: tendencias});
    }).catch((error)=> {
        return res.status(404).json(error).send();
    });
};