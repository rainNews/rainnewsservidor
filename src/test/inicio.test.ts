const supertest = require("supertest");
const app = require("../index");
const chai = require("chai");

chai.should();
const expect = chai.expect;
const api = supertest.agent(app);

describe("/post registroUsuario", () => {
    it("should not add a new user", (done) => {
        api
            .post("/registroUsuario")
            .send(
                {
                    correo: 'pruebacorreo',
                    nombre: 'prueba',
                    apellido: 'prueba',
                    password: 'prueba'
                }
            ).end(function (err: any, res: any) {
                if (err) {
                    done(err);
                }
                done();
            });
    });
});

describe("/post inicio", () => {
    it("should connect to the app", (done) => {
        api
            .post("/inicio")
            .send(
                {
                    correo: "pepito",
                    password: "pepito"
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post noticias usuario", () => {
    it("should connect to the app", (done) => {
        api
            .post("/postNoticiasUsuario")
            .send(
                {
                    id: "pepito"
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post timeline", () => {
    it("should connect to the app", (done) => {
        api
            .post("/timeline")
            .send(
                {
                    id: "pepito" // id del usuario
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post tema", () => {
    it("should connect to the app", (done) => {
        api
            .post("/temasUsuario")
            .send(
                {
                    id: 1 // id del usuario
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post me gusta en publicacion", () => {
    it("should connect to the app", (done) => {
        api
            .post("/postNoticiaUsuariosLikes")
            .send(
                {
                    id: 1 // id de noticia
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post modificar perfil", () => {
    it("should not add a new user", (done) => {
        api
            .post("/postUsuario")
            .send(
                {
                    correo: 'pruebacorreo',
                    nombre: 'prueba',
                    apellido: 'prueba',
                    password: 'prueba'
                }
            ).end(function (err: any, res: any) {
                if (err) {
                    done(err);
                }
                done();
            });
    });
});

describe("/get comentario", () => {
    it("should connect to the app", (done) => {
        api
            .post("/getComentario")
            .send(
                {
                    id: "1"
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post comentarios", () => {
    it("should connect to the app", (done) => {
        api
            .post("/postComentarios")
            .send(
                {
                    id: "1"
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post comentario", () => {
    it("should connect to the app", (done) => {
        api
            .post("/postComentario")
            .send(
                {
                    id: "1"
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post UpdateComentario", () => {
    it("should connect to the app", (done) => {
        api
            .post("/postUpdateComentario")
            .send(
                {
                    id: "1"
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post delete noticia usuario", () => {
    it("should connect to the app", (done) => {
        api
            .post("/deleteNoticiaUsuario")
            .send(
                {
                    userid: "pepito",
                    noticiaid: 1
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post update noticia", () => {
    it("should connect to the app", (done) => {
        api
            .post("/postUpdateNoticia")
            .send(
                {
                    id: 1
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});

describe("/post enviar correo noticia usuario", () => {
    it("should connect to the app", (done) => {
        api
            .post("/postNoticiaUsuario")
            .send(
                {
                    id: 1
                }
            )
            .end((err: any, res: any) => {
                if (err) {
                    done(err)
                }
                done();
            });
    });
});
