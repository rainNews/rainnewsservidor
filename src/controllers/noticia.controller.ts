import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Usuario } from "../entity/Usuario";
import { Tema } from "../entity/Tema";
import { Noticia } from "../entity/Noticia";

import nodemailer from "nodemailer";

export const postNoticiasUsuario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .leftJoinAndSelect("usuario.noticias", "noticias")
                .leftJoinAndSelect("noticias.usuarios", "usuarios")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();

            if (usuario) {
                return res.status(200).send({ noticias: usuario.noticias });
            }
            return res.status(404).send({ mensaje: "no existe usuario" });
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const deleteNoticiaUsuario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            await connection
                .createQueryBuilder()
                .relation(Usuario, "noticias")
                .of([{ userid: req.body.userid }])
                .remove({ noticiaid: req.body.noticiaid });
            return res.status(200).send();
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const postNoticiaUsuario = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection
        .then(async (connection) => {
            const tema = await connection
                .getRepository(Tema)
                .createQueryBuilder("tema")
                .leftJoinAndSelect("tema.noticias", "noticias")
                .where("tema.temaid = :id", { id: req.body.temaid })
                .getOne();
            if (!tema) {
                return res.status(404).send({ mensaje: "no existe tema" });
            }
            const usuario = await connection
                .getRepository(Usuario)
                .createQueryBuilder("usuario")
                .leftJoinAndSelect("usuario.noticias", "noticias")
                .where("usuario.userid = :id", { id: req.body.userid })
                .getOne();
            if (!usuario) {
                return res.status(404).send({ mensaje: "no existe usuario" });
            }

            const noticia = new Noticia();
            noticia.noticia = req.body.noticia;
            await connection.manager.save(noticia);

            usuario.noticias.push(noticia);
            await connection.manager.save(usuario);

            tema.noticias.push(noticia)
            await connection.manager.save(tema);

            let transport = nodemailer.createTransport({
                host: 'smtp.mailtrap.io',
                port: 2525,
                auth: {
                    user: "4f3a6db235ec5e",
                    pass: "d6f1fe40074a09"
                }
            });

            const message = {
                from: 'servidor@rainnews.com', // Sender address
                to: usuario.correo,         // List of recipients
                subject: 'Aviso de publicación de noticia', // Subject line
                text: noticia.noticia // Plain text body
            };
            transport.sendMail(message, function(err, info) {
                if (err) {
                  console.log(err)
                } else {
                  console.log(info);
                }
            });
            return res.status(200).send();
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const postUpdateNoticia = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection.
        then(async (connection) => {
            const noticia = await connection
                .getRepository(Noticia)
                .createQueryBuilder("noticia")
                .where("noticia.noticiaid = :id", { id: req.body.noticiaid })
                .getOne();

            if (!noticia) {
                return res.status(404).send({ mensaje: "no existe noticia" });
            }
            noticia.noticia = req.body.noticia;
            await connection.manager.save(noticia);
            return res.status(200).send();
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

export const getNoticia = async (
    req: Request,
    res: Response
): Promise<Response> => {
    return connection.
        then(async (connection) => {
            const noticia = await connection
                .getRepository(Noticia)
                .createQueryBuilder("noticia")
                .where("noticia.noticiaid = :id", { id: req.body.noticiaid })
                .getOne();

            if (!noticia) {
                return res.status(404).send({ mensaje: "no existe noticia" });
            }
            return res.status(200).send({ noticia: noticia });
        })
        .catch((error) => {
            return res.status(404).json(error).send();
        });
};

